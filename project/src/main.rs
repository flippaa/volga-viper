extern crate pancurses;
extern crate rand;

mod menu;

use rand::Rng;
use std::io::Write;
use pancurses::*;

const SNAKE_COLOR_ID: u16 = 1;
const FOOD_COLOR_ID: u16 = 2;
const SNAKE_COLOR: i16 = COLOR_RED;
const FOOD_COLOR: i16 = COLOR_GREEN;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct Position(i32, i32);

impl Position {
    fn move_by(&self, velocity: Position, size: Position) -> Position {
        let x = (self.0 + velocity.0 + size.0) % size.0;
        let y = (self.1 + velocity.1 + size.1) % size.1;
        Position(x, y)
    }
}

#[derive(Debug)]
struct State {
    player: Position,
    velocity: Position,
    size: Position,
    tail: Vec<Position>,
    length: usize,
    food: Position,
}

impl State {
    fn new(width: i32, height: i32) -> Self {
        State {
            player: Position(10, 10),
            velocity: Position(0, 1),
            size: Position(width, height),
            tail: Vec::new(),
            length: 5,
            food: Position(20, 20),
        }
    }

    fn tick(&mut self) {
        self.tail.push(self.player);
        if self.tail.len() > self.length {
            self.tail.remove(0);
        }

        self.player = self.player.move_by(self.velocity, self.size);

        if self.is_dead() {
            self.reset();
        }

        if self.player == self.food {
            self.length += 1;
            self.food = self.random_position_on_board();
        }
    }

    fn is_dead(&self) -> bool {
        self.tail.iter().any(|&position| position == self.player)
    }

    fn reset(&mut self) {
        self.length = 5;
        self.tail.clear();
    }

    fn random_position_on_board(&self) -> Position {
        let mut rng = rand::thread_rng();
        Position(rng.gen_range(0, self.size.0), rng.gen_range(1, self.size.1))
    }

    fn input(&mut self, input: Input) {
        use Input::*;

        self.velocity = match input {
            KeyUp    => Position(0, -1),
            KeyDown  => Position(0, 1),
            KeyLeft  => Position(-1, 0),
            KeyRight => Position(1, 0),
            _ => return,
        }
    }

    fn render(&self, window: &Window) {
        self.render_snake(window);
        self.render_food(window);
    }

    fn render_snake(&self, window: &Window) {
        window.attrset(COLOR_PAIR(SNAKE_COLOR_ID as u64));

        for position in &self.tail {
            window.mvaddch(position.1, position.0, '#');
        }
        window.mvaddch(self.player.1, self.player.0, '#');
    }

    fn render_food(&self, window: &Window) {
        window.attrset(COLOR_PAIR(FOOD_COLOR_ID as u64));
        window.mvaddch(self.food.1, self.food.0, '#');
    }
}

fn game_start() {
    let window = initscr();
    let result = std::panic::catch_unwind(|| {
        start_color();
        init_pair(SNAKE_COLOR_ID as i16, SNAKE_COLOR, SNAKE_COLOR);
        init_pair(FOOD_COLOR_ID as i16, FOOD_COLOR, FOOD_COLOR);
        nl();
        noecho();
        curs_set(0);

        window.nodelay(true);
        window.keypad(true);

        let mut state = State::new(window.get_max_x(), window.get_max_y());

        let sleep_time = std::time::Duration::from_millis(100);

        loop {
            window.clear();
            state.render(&window);
            window.refresh();

            std::thread::sleep(sleep_time);

            state.tick();

            if let Some(keypress) = window.getch() {
                state.input(keypress);
            }
        }
    });
    endwin();

    if let Err(e) = result {
        if let Some(e) = e.downcast_ref::<&'static str>() {
            writeln!(&mut std::io::stderr(), "Error: {}", e).unwrap();
        } else {
            writeln!(&mut std::io::stderr(), "Unknown error: {:?}", e).unwrap();
        }
        std::process::exit(1);
    }
}

fn main() {
    game_start();
}