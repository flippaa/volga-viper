extern crate pancurses;

use pancurses::*;

fn input(i: &mut i32, input: Input) -> bool {
    match input {
        Input::KeyUp => {
        *i -= 1;
        if *i < 0 {
            *i = 3;
        }
        return true;
        }, 
        Input::KeyDown => {
            *i += 1;
            if *i > 3 {
                *i = 0;
            }
            return true;
        },
        _ => { false }    
    }
}

fn menu() -> i32 { 
    let items = ["One", "Two", "Three", "Four"];
    let window = initscr();
    for (i, item) in items.iter().enumerate()  {
	  if *item == "One" {
        window.attron(A_REVERSE);
	  }
	  else {
	    window.attroff(A_REVERSE);
      }
	  window.mvprintw(1+(i as i32), 10, item);	
    }
    noecho();
    window.nodelay(true);
    window.keypad(true);
    //curs_set(0);
    let mut i :i32 = 0;
    loop {
        if let Some(keypress) = window.getch() {
            if input(&mut i, keypress) {
                window.attroff(A_REVERSE);
                window.mvprintw(1 + i, 10, items[(i as usize)]);
                window.attron(A_REVERSE);
            }
       
        }
    }
    endwin();
    i
}


